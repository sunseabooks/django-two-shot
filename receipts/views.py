from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from .models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from .forms import ReceiptForm, ExpenseForm, AccountForm


@login_required()
def receiptList(request):
    receipts = Receipt.objects.filter(purchaser=request.user.id)
    context = {"receipts": receipts}
    return render(request, "receipts/list.html", context)


@login_required
def expenseCategoryList(request):
    # new = request.user.categories.all()
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": categories,
    }
    return render(request, "categories/list.html", context)


@login_required
def accountList(request):
    accounts = Account.objects.filter(owner=request.user.id)
    context = {
        "accounts": accounts,
    }
    return render(request, "accounts/list.html", context)


@login_required
def receiptCreate(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect(reverse_lazy("home"))
    else:
        form = ReceiptForm()
    context = {"form": form}
    return render(request, "receipts/create.html", context)


@login_required
def expenseCategoryCreate(request):
    if request.method == "POST":
        form = ExpenseForm(request.POST)
        if form.is_valid():
            expense = form.save(commit=False)
            expense.owner = request.user
            expense.save()
            return redirect(reverse_lazy("list_categories"))
    else:
        form = ExpenseForm
    context = {"form": form}
    return render(request, "categories/create.html", context)


@login_required
def accountCreate(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect(reverse_lazy("list_accounts"))
    else:
        form = AccountForm
    context = {"form": form}
    return render(request, "accounts/create.html", context)
