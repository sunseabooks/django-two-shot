from django.urls import path
from .views import (
    receiptList,
    receiptCreate,
    expenseCategoryCreate,
    accountCreate,
    expenseCategoryList,
    accountList,
)

urlpatterns = [
    path("", receiptList, name="home"),
    path("create/", receiptCreate, name="create_receipt"),
    path("categories/", expenseCategoryList, name="list_categories"),
    path("accounts/", accountList, name="list_accounts"),
    path("categories/create/", expenseCategoryCreate, name="create_category"),
    path("accounts/create/", accountCreate, name="create_account"),
]
